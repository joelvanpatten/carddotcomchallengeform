<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ContactController extends Controller
{
    /**
     * Shows the contact form page.
     */
    public function show()
    {
        $data = [];
        return view('contact', $data);
    }

    public function submit(Request $request)
    {
        $input = $request->all();

        // TODO - Make sure I use server-side validation as well. Javascript validation provides a great user-experience,
        //        but it is no substitute for server-side validation.

        // Most or all of this should be taken care of by Laravel, but the usual disclaimers apply:
        // TODO - Make sure input is sanitized before adding to a database or displaying to the browser.
            // Eloquent ORM can prevent SQL injection by using parameter binding.

            // using {{ $variable }} notation in your blade templates will ensure $variable is automatically
            // sent through PHP's htmlspecialchars function to prevent other common attacks.


        // Just fake a submission number for now.  If this were a real app we would probably save the form to a database.
        $data = [
            'submitted' => $input,
            'submissionId' => uniqid()
        ];
        return view('submitted', $data);
    }
}