@extends('layouts.app')

@section('title' , 'Contact Form Demo')

@section('content')
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title alert alert-warning" role="alert" id="exampleModalLabel">Confidential Information Detected</h5>
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                </div>
                <div class="modal-body">
                    We believe that you might have submitted a Social Security Number (SSN), a Personal Account Number (PAN) or other confidential information.  We take
                    your privacy very seriously.  Do not include any SSN, PAN or other confidential information in your
                    form submission.  Please take a moment to edit your submission to remove any confidential details.  Thank you.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Edit My Form Submission</button>
                    <button id="sendAnyway" type="button" class="btn btn-danger">Send The Form As Is</button>
                </div>
            </div>
        </div>
    </div>

    <h1>Contact Form Demo</h1>

    <form id="formDemo" method="POST" action="/contact-submit">
        @csrf
        <div class="form-row">
            <div class="form-group col-md-6 CF-form-group">
                <label for="inputName">Your Name</label>
                <input type="text" class="form-control" name="name" id="inputName" placeholder="Your Name">
                <div class="CF-error-message"></div>
            </div>
            <div class="form-group col-md-6 CF-form-group">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email">
                <div class="CF-error-message"></div>
            </div>
        </div>
        <div class="form-group CF-form-group">
            <label for="inputStory">Tell us your story</label>
            <textarea class="form-control" id="inputStory" name="story" rows="3"></textarea>
            <div class="CF-error-message"></div>
        </div>
        <input id="formDemoSubmit"type="submit" class="btn btn-primary" value="Submit">
        {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    </form>

<script type="text/javascript">

    // If you need to do some quick and dirty pattern testing you can use the following code.
    // Make changes as necessary and then refresh the page and look at your dev console to see the
    // regex test results.
    // var pattern = /(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})/;
    // console.log(pattern.test("4494-6540-9999-9999"));
    // console.log(pattern.test("4494 6540 9999 9999"));
    // console.log(pattern.test("4494654099999999"));

    // Using validate.js.  Additional constraints can certainly be added.
    // see https://validatejs.org/
    var constraints = {
        name: {
            presence: true
        },
        email: {
            presence: true
        },
        story: {
            presence: true
        }
    };

    $('#formDemoSubmit').on('click', function (e) {
        e.preventDefault();
        var form = $('#formDemo');
        jvp.handleFormSubmit(form, constraints, '#exampleModal');
    });

    $('#sendAnyway').on('click', function (e) {
        var form = $('#formDemo');
        jvp.submitTheForm(form, constraints);
    });
</script>
@endsection