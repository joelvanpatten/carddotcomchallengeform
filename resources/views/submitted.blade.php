@extends('layouts.app')

@section('title' , 'Contact Form Demo')

@section('content')
<br>
<br>
<div class="card">
    <div class="card-header">
        Thank you for your submission.
    </div>
    <div class="card-body">
        <h5 class="card-title">Your Submission Details</h5>
        <p class="card-text">
            @foreach ($submitted as $key => $item)
                @if($key != '_token')
                    <p>{{ ucfirst($key) }} : {{ $item }}</p>
                @endif
            @endforeach
        </p>
    </div>
    <div class="card-footer">
        If you have any questions please contact customer support at (555) 555-5555.  Your form submission number is {{ $submissionId }}.
        <br>
        <br>
        <br>
        <a target="_self" href="/contact">Return to the contact form.</a>
    </div>
</div>
@endsection