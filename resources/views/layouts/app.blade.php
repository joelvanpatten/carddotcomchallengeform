<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

    {{-- TODO - install a local version of validate js --}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.12.0/validate.min.js"></script>

    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/jvp.js')}}"></script>


    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="{{asset('css/jvp.css')}}">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-8">
            @yield('content')
        </div>
        {{--<div class="col-4">--}}
            {{--@section('sidebar')--}}
                {{--This is the master sidebar.--}}
            {{--@show--}}
        {{--</div>--}}
    </div>
</div>
</body>
</html>