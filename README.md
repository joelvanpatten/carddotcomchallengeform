





## Items to Consider
- Do we anticipate that we are going to ever want to disable this feature?
- Do we desire fine grained control?
    - In other words, do we want to be able to check for a TIN but ignore SSN.  
    - Do we want separate error messages for each check.
- Should we allow the user to proceed anyway (in case of false positives or something).
    - Are there business consequences (fines, liability, etc.) if we accidentally store a customer's Personally Identifiable Informaiton (PII). 
    - What are the quantifiable business consequences (lost revenue, decreased Net promoter score, etc.) if the form is too slow or too difficult or too restrictive for customers to use.



## App Notes
- Although a best effort was made to ensure that the form is responsive (i.e. mobile friendly) and cross-browser compatible, no guarantees are provided.
- It has not been thoroughly tested, it may contain bugs or lack desired features.
- It was minimally tested on Google Chrome Version 70.0.3538.110 (Official Build) (64-bit).
- The app performs a regular validation first, then if the first validation passes it will check all the form fields to see if they contain a SSN.


## Getting Started
If you have Docker installed on your development computer you can follow these steps:

1) In Docker terminal window navigate to your desired development directory and enter:  
    ```git clone https://joelvanpatten@bitbucket.org/joelvanpatten/larafunk.git```

2) Navigate to the **larafunk** directory.  
    ```cd larafunk```

3) Copy the example environment file.  
    ```cp .env.example .env```  
    This is a demo application, so no changes to the .env file are required.  If this were a real application you would want to edit the .env file to fit your desired environment.  At a minimum you would want to change the APP_KEY.  Remember not to include passwords or sensitive account information in any file that is in version control (the .env file should remain in your .gitignore file list so that git will not add it to the repository).
    
4) Run docker-compose:  
    ```docker-compose up -d```  
    It will take a few moments for Docker to pull the desired images and build your containers.  Even after it appears that Docker is done, Composer is likely still pulling project dependencies into the container.  **Please allow a few moments for Docker to do its thing.**

5) If docker completed with no errors you should be able to view the demo application at [http://0.0.0.0:3000](http://0.0.0.0:3000).  
If you are using Docker Toolbox for Windows you may need to use [http://192.168.99.100:3000](http://192.168.99.100:3000).  
You could also try [http://localhost:3000](http://localhost:3000).

**Troubleshooting:**    
You can view the status of containers by first getting the container id:  
```docker ps```  
Then you can use the first few characters of the container id to view the container logs.  If your Laravel container id is **cc898eb10af**, then the command to view logs would be:  
```docker logs cc898e```  
You just need enough of the container id to uniquely identify it.

## Checking for Social Security Numbers (SSN)
Regular Expressions is not something I just know off the top of my head. I usually search Google or StackOverflow for a particular regex pattern (i.e. "Regex for SSN") and I can check the answer using [regex101.com](https://regex101.com/)


The regular expression for social security number (SSN) is

**/[0-9]{3}\-?[0-9]{2}\-?[0-9]{4}/**


- **/[0-9]{3}**:  Means that the social security number must begin with at least three numeric characters.
- **\\-?**:   Means that SSN can have an optional hyphen (-) after first 3 digits.
- **[0-9]{2}**: First 3 digits (or optional hyphen) must be followed by 2 more digits.
- **\\-?**: After the second group of digits there may be another optional hyphen (-) character.
- **[0-9]{4}/**: Finally, the social security number must end with four digits.

## SSN Examples:
- Following will be flagged as SSNs : 
    - 345-90-98023
    - 234908908
    - 239-586767
    - 73467-8790
- The following will not be flagged as SSNs. 
    - 34-909-9098 
    - 34a-20-7678
    
## Checking for Credit Card Numbers (Personal Account Numbers or PAN)  
Credit card issuers come and go.  Credit card issuers sometimes change their PAN format.  For a discussion of possible regular expressions for credit card numbers see the following links:
- [https://stackoverflow.com/questions/9315647/regex-credit-card-number-tests](https://stackoverflow.com/questions/9315647/regex-credit-card-number-tests)
- [https://www.regular-expressions.info/creditcard.html](https://www.regular-expressions.info/creditcard.html)

The regex I chose is good for a basic "does it look like a card number".
It would certainly be possible to do a more exhaustive search against known Credit Card formats
(i.e. Amex, Visa, etc).  The problem then becomes ensuring that the card formats are kept up to date
and that the pattern matching does not begin to take too long to process.

The PAN regex looks for a string of digits that is between 13 and 16 digits long, with any amount of dashes or
spaces between the digits.
    
    
## Possible Improvements
- It may have been better to use ajax based validation and form submission.  
    - That way if it fails server-side validation (which has not been built yet), it will provide a better UX because they won't have to wait for the page reload to see the submission errors.
- Look for ways to make the validation code more generic (less tightly coupled to the form).