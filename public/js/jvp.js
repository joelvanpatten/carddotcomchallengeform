/**
 * @depends jQuery
 * @depends validate.js
 *
 */
jvp = function () {
    var handleFormSubmit = function (form, constraints, modalId) {
        var values = validate.collectFormValues(form);
        var $hasErrors = failedStandardValidation(form, constraints, values);

        if($hasErrors){// the failedStandardValidation() function will display the errors if they are found.
            return true;
        }

        var $piiExists = hasPii(values, constraints);

        if($piiExists){
            // console.log("pii check failed");
            // console.log($piiExists);
            $(modalId).modal();
        } else {
            // console.log("pii check passed");
            // console.log($piiExists);
            submitTheForm(form);
        }
    };

    var failedStandardValidation = function(form, constraints, values){
        var errors = validate(values, constraints);

        if(errors){
            showErrors(form, errors || {});
            return true;
        }

        return false;
    };

    var submitTheForm = function (form) {
        /* We should have already validated everything except if it contains PII.
           However, if needed we could do:

                var values = validate.collectFormValues(form);
                var $hasErrors = failedStandardValidation(form, constraints, values);
                if($hasErrors){
                    return true;
                }
        */

        $(form).submit();
    };

    /**
     * Determines if the form has a SSN or some other type of Personally Identifiable Information (PII)
     */
    var hasPii = function (values, constraints) {
        // Create custom validator function
        validate.validators.pii = function(value, options, key, attributes) {
            // console.log(value);
            // console.log(options);
            // console.log(key);
            // console.log(attributes);

            // if value is a SSN then return something other than null or undefined.

            // Regular Expressions is not something I just know off the top of my head.
            // I usually search Google or StackOverflow for a particular regex pattern
            // (i.e. "Regex for SSN") and I can check the answer using https://regex101.com/
            // see README.md at the root of this project for a more thorough explanation.

            if(options.ssn){
                console.log("Checking for SSN.");

                // var  ssnPattern = /^[0-9]{3}\-?[0-9]{2}\-?[0-9]{4}$/; // This only works if it the SSN is isolated.
                var ssnPattern = /[0-9]{3}\-?[0-9]{2}\-?[0-9]{4}/;      // This works if the SSN is surrounded by other text.
                if(ssnPattern.test(value)){
                    // Validate.js expects us to return an error message.
                    return "contains a possible SSN.";
                }
            }

            // Ensure they are not submitting Credit Card numbers.
            if(options.pan){
                console.log("Checking for PAN");

                // Note: Credit card issuers come and go.  Credit card issuers sometimes change their PAN format.
                // See -- https://stackoverflow.com/questions/9315647/regex-credit-card-number-tests.
                // See -- https://www.regular-expressions.info/creditcard.html

                // This regex is good for a basic "does it look like a card number".
                // It would certainly be possible to do a more exhaustive search against known Credit Card formats
                // (i.e. Amex, Visa, etc).  The problem then becomes ensuring that the card formats are kept up to date
                // and that the pattern matching does not begin to take too long to process.

                // Looks for a string of digits that is between 13 and 16 digits long, with any amount of dashes or
                // spaces between the digits.
                var panPattern = /(?:\d[ -]*?){13,16}/;
                if(panPattern.test(value)){
                    // Validate.js expects us to return an error message.
                    return "contains a possible SSN.";
                }
            }

            // We can add other checks here if needed
            // i.e. Tax Id Number for businesses or other forms of Personally Identifiable Information (PII)

            return null;
        };

        // add custom validation to each constraint
        $.each(constraints, function(i, constraint) {
            constraint.pii = {"ssn":true, "pan":true};
            // constraint.pii = {};
            // console.log(i);
            // console.log(constraint);
        });

        // Validate the form.
        var piiErrors = validate(values, constraints);

        // remove the constraints, in case we want to allow user to submit the form anyway.
        $.each(constraints, function(i, constraint) {
            constraint.pii = {};
        });

        return piiErrors;
    };

    var showErrors = function(form, errors) {
        $('input[type="text"], input[type="email"], input[type="file"], select, textarea').each(function(i, v){
            showErrorsForInput($(v), errors && errors[$(v).attr('name')]);
        });
    };

    var showErrorsForInput = function(input, errors) {
        // This is the root of the input
        var formGroup = input.closest(".CF-form-group");
        var messages = formGroup.find(".CF-error-message");

        resetFormGroup(formGroup);

        if (errors) {
            formGroup.addClass("CF-has-error");
            $.each(errors, function(i, error) {
                addError(messages, error);
            });
        } else {
            formGroup.addClass("CF-has-success");
        }
    };

    var resetFormGroup = function(formGroup) {
        formGroup.removeClass("CF-has-error");
        formGroup.removeClass("CF-has-success");

        formGroup.find(".CF-help-block.CF-error").each(function(i, el) {
            $(el).remove();
        });
    };

    var addError = function(messages, error) {
        var block = $('<p>');
        block.addClass("CF-help-block");
        block.addClass("CF-error");
        block.html(error);
        messages.append(block);
    };

    return {
        handleFormSubmit: handleFormSubmit,
        submitTheForm: submitTheForm
    };
}();

